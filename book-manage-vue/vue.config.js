const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 解除vue3组件名多词限制
  lintOnSave:false,
  // client: {
  //   webSocketURL: 'ws://0.0.0.0:8999/ws'
  // }
})
