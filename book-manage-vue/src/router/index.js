import {createRouter, createWebHistory} from "vue-router";
import store from "@/store";
import staticRoute from "@/router/static-route";
// import Layout from "@/views/Layout";
// import Index from "@/views/Index";
// import Book from "@/views/Book";
// import User from "@/views/User";
// import Menu from "@/views/Menu";
// import NotFount from "@/views/NotFount";
// import store from '../store'
// import staticRoute from './static-route.js'
// import dyRoutes from "@/router/dyRoutes";
// import NotFound from "@/views/NotFound";
// import data from "@/api/menu";

// const router = createRouter({
//     history: createWebHistory(),
//     base: process.env.BASE_URL,
//     routes: staticRoute //staticRoute为静态路由，不需动态添加
// })

// 初始化路由
export const routes = [
    {
        path: "/account",
        name: "Account",
        meta: {
            requiresAuth: false
        },
        component: () => import("../views/Account")
    },
    {
        meta: {
            title: "图书管理",
            keepAlive: true
        },
        path: '',         // uri访问地址
        name: "Layout",
        component: () => import("../views/Layout"),
        redirect: "/index",
        children: [
            {
                meta: {
                    title: "主页",
                    keepAlive: true
                },
                path: '/index',
                name: 'Index',
                component: () => import("../views/Index"),
            },
            {
                meta: {
                    title: "书籍管理",
                    keepAlive: true
                },
                path: '/book',
                name: 'Book',
                component: () => import("../views/Book")
            },
            {
                meta: {
                    title: "借阅管理",
                    keepAlive: true
                },
                path: '/lend',
                name: 'Lend',
                component: () => import("../views/Lend")
            },
            {
                meta: {
                    title: "个人中心",
                    keepAlive: true,
                },
                path: "/userCenter",
                name: "UserCenter",
                component: () => import("../views/UserCenter")
            },
            {
                meta: {
                    title: "系统管理",
                    keepAlive: true
                },
                path: '/sys',
                name: 'System',
                redirect: "sys/user",
                children: [
                    {
                        meta: {
                            title: "用户管理",
                            keepAlive: true
                        },
                        path: '/sys/user',
                        name: 'User',
                        component: () => import("../views/sys/User")
                    },
                    {
                        meta: {
                            title: "角色管理",
                            keepAlive: true
                        },
                        path: '/sys/role',
                        name: 'Role',
                        component: () => import("../views/sys/Role")
                    },
                    {
                        meta: {
                            title: "菜单管理",
                            keepAlive: true
                        },
                        path: '/sys/menu',
                        name: 'Menu',
                        component: () => import("../views/sys/Menu")
                    }
                ]
            },
        ]
    },
    {
        path: '/notfound',
        name: 'nothing',
        meta: {
            requiresAuth: false
        },
        component: () => import('../views/NotFound')
    },

]

export const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

console.log("**", router.getRoutes());
console.log("hasRoute:", router.hasRoute("Book"))


export default router;