const staticRoute = [
    {
        meta: {
            title: "主页",
            keepAlive: true
        },
        path: '/index',
        name: 'Index',
        component: () => import("../views/Index"),
    },
    {
        meta: {
            title: "书籍管理",
            keepAlive: true
        },
        path: '/book',
        name: 'Book',
        component: () => import("../views/Book")
    },
    {
        meta: {
            title: "个人中心",
            keepAlive: true,
        },
        path: "/userCenter",
        name: "UserCenter",
        component: () => import("../views/UserCenter")
    },
    {
        meta: {
            title: "系统管理",
            keepAlive: true
        },
        path: '/sys',
        name: 'System',
        redirect: "sys/user",
        children: [
            {
                meta: {
                    title: "用户管理",
                    keepAlive: true
                },
                path: '/sys/user',
                name: 'User',
                component: () => import("../views/sys/User")
            },
            {
                meta: {
                    title: "菜单管理",
                    keepAlive: true
                },
                path: '/sys/menu',
                name: 'Menu',
                component: () => import("../views/sys/Menu")
            }
        ]
    },
]
export default staticRoute;
