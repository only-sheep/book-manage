//动态路由
const dynamicRoute = [
    {
        meta: {
            title: "图书管理",
            keepAlive: true
        },
        path: '/',         // uri访问地址
        name: "layout",
        component: () => import("../views/Layout"),
        children: [
            {
                path: '',
                redirect: "account",
            },
            {
                meta: {
                    title: "主页",
                    keepAlive: true
                },
                path: '/index',
                name: 'Index',
                component: () => import("../views/Index")
            },
            {
                meta: {
                    title: "书籍管理",
                    keepAlive: true
                },
                path: '/book',
                name: 'Book',
                component: () => import("../views/Book")
            },
            {
                meta: {
                    title: "用户管理",
                    keepAlive: true
                },
                path: '/user',
                name: 'User',
                component: () => import("../views/sys/User")
            },
            {
                meta: {
                    title: "菜单管理",
                    keepAlive: true
                },
                path: '/menu',
                name: 'Menu',
                component: () => import("../views/sys/Menu")
            }
        ]
    },
]
export default dynamicRoute
