import {router} from "./index"
import store from "../store/index"
// let isAdd = false;
// router.beforeEach((to, from, next) => {
//     if (to.fullPath === "/account") {
//         next();
//     } else {
//         if (!isAdd) {
//             data.get_all_menu().then(res => {
//                 let info = res.data
//                 if (info.code === 20041) {
//                     let menus = info.data.nav;
//                     menus.forEach(menu => {
//                         if (menu.type === 1) {
//                             let route = menuToRoute(menu);
//                             console.log(route)
//                             router.addRoute("layout", route)
//                         }
//                         if (menu.type === 0) {
//                             let route = menuToRoute2(menu);
//                             console.log(route)
//                             router.addRoute("layout", route)
//                             menu.children.forEach(e => {
//                                 //转换路由
//                                 let route = menuToRoute(e);
//                                 //添加到管理器中
//                                 if (route) {
//                                     console.log(route)
//                                     router.addRoute(menu.name, route)
//                                 }
//                             })
//                         }
//                     })
//                 }
//             }).catch(err => {
//                 console.log(err)
//             })
//             // router.addRoute("layout", {
//             //     meta: {
//             //         title: "书籍管理",
//             //         keepAlive: true
//             //     },
//             //     path: '/book',
//             //     name: 'Book',
//             //     component: () => import("../views/Book")
//             // })
//             isAdd = true
//
//             next();
//             console.log(router.getRoutes());
//             // next({
//             //     ...to, // next({ ...to })的目的,是保证路由添加完了再进入页面 (可以理解为重进一次)
//             //     replace: true, // 重进一次, 不保留重复历史
//             // })
//
//         }
//     }
//
//
// })
// const menuToRoute = (menu) => {
//     if (!menu.component) {
//         return null
//     }
//     // 复制属性
//     let route = {
//         name: menu.name,
//         path: menu.path,
//         meta: {
//             icons: menu.icons,
//             title: menu.title
//         }
//     }
//     route.component = () => import('@/views/' + menu.component + '.vue');
//     // route.component = (resolve) => require([`@/views/${menu.component}`], resolve);
//     return route
// }
// const menuToRoute2 = (menu) => {
//     // 复制属性
//     return {
//         name: menu.name,
//         path: menu.path,
//         redirect: menu.children[0].path
//     }
// }
const whiteRouter = ["Account"]

router.beforeEach((to, from, next) => {
        const token = localStorage.getItem("jwt_token")
        console.log(router.fullPath);
        if (token) {
            if (to.path === "/account") {
                next({path: "/"})
            }

            if (store.state.permission.all_router.length === 0) {
                store.dispatch("permission/actionGetPermissionMenu").then(() => {
                    // 更新静态路由
                    router.options.routes = store.state.permission.all_router;
                    // 更新动态路由
                    store.state.permission.asynd_router.forEach(route => {
                        console.log("添加路由...")
                        console.log(route)
                        router.addRoute("Layout", route)
                    })
                    // staticRoute.forEach(route => {
                    //     console.log("添加路由...")
                    //     console.log(route)
                    //     router.addRoute("Layout", route)
                    // })

                    localStorage.setItem('permission', JSON.stringify(store.state.permission.all_router));
                    // router.addRoute(store.state.permission.asynd_router)
                    console.log(router.getRoutes())
                    console.log("hasRoute:", router.hasRoute("Book"))

                    next()
                });
            }
        } else {
            if (whiteRouter.includes(to.name)) {
                next()
            } else {
                next({name: "Account"})
            }
        }

    }
)
