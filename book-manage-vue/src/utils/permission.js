import store from "@/store/index";

//先获取本地权限数据 permissions
let btnPermission = data => {
    // 从vuex中取出permission信息
    if (!store.state.account.UserPermission) {
        return false
    }
    let permissions = store.state.account.UserPermission.split(",");
    // console.log(permissions)
    // let permissions = localStorage.getItem('permissions').split(",");

    for (let i = 0; i < permissions.length; i++) {
        if (permissions[i] === data) {
            return true
        }
    }
    return false;
}
export default btnPermission;