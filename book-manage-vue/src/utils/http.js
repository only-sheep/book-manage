import axios from "axios"
import router from "@/router";
import store from "@/store/index";

const http = axios.create({
    // timeout: 2500,                          // 请求超时，有大文件上传需要关闭这个配置
    baseURL: "http://127.0.0.1:8886/api",     // 设置api服务端的默认地址[如果基于服务端实现的跨域，这里可以填写api服务端的地址，如果基于nodejs客户端测试服务器实现的跨域，则这里不能填写api服务端地址]
    withCredentials: false,                    // 是否允许客户端ajax请求时携带cookie

})

http.interceptors.request.use(config => {
    // let token = window.localStorage.getItem("jwt_token");
    // console.log("this--->", this)
    let token = store.state.account.UserToken;

    // let token = store.state.jwt_token;
    if (token) {
        config.headers.token = token;
    }
    return config;
}, function (error) {
    this.$router.push("/account")
    return Promise.reject(error)
})

http.interceptors.response.use(function (response) {
    if (response.data.code === 40000) {
        router.push("/account")
    }
    return response;
}, function (error) {
    return Promise.reject(error);
})

export default http;