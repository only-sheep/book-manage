import {createApp} from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import router from './router/index'
import 'element-plus/dist/index.css'
import './utils/permission.js'
import btnPermission from "@/utils/permission";
import store from './store/index'
// import './assets/icons/iconfont.css'
// import "./router/permission"


let app = createApp(App)

// 挂载全局方法
app.config.globalProperties.$has = btnPermission;

app.config.devtools = true

app.use(ElementPlus).use(router).use(store).mount('#app');

