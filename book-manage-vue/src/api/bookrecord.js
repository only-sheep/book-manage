import http from "../utils/http"
import {reactive} from "vue"

const data = reactive({
    get_lend_record_info(currentPage, sizePage) {
        return http.get(`/bookRecord/${currentPage}/${sizePage}`)
    },
})
export default data;
