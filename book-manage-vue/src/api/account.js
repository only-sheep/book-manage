import http from "../utils/http"
import {reactive} from "vue"

const data = reactive({
    // 登录接口
    sign_in(loginForm) {
        return http.post("/user/login", loginForm)
    },
    // 登出接口
    sign_out() {
        let token = window.localStorage.getItem("jwt_token")
        return http.get("/user/logout", {
            headers: token
        })
    },

    // 注册接口
    sign_up(registerForm) {
        return http.post("/register", registerForm)
    },
    // 获取用户权限
    get_permission_list() {
        return http.get("/user/permission")
    }

})
export default data;
