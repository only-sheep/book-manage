import http from "../utils/http"
import {reactive} from "vue"

const data = reactive({
    get_all_book(currentPage, sizePage, input_search_key, select_search_key) {
        return http.get(`/books/${currentPage}/${sizePage}` + "?input_search_key=" + input_search_key + "&select_search_key=" + select_search_key
        )
    },
    update_book_info(formData) {
        return http.put("/books", formData)
    },
    save_book_info(formData) {
        const token = localStorage.getItem("jwt_token");
        return http.post("/books", formData, {
            headers: {
                token: token
            }
        });
    },
    remove_book_info(id) {
        return http.delete(`/books/${id}`)
    },
    get_type() {
        return http.get("/books/type"
        )
    },

})
export default data;
