import http from "../../utils/http"
import {reactive} from "vue"

const data = reactive({
    get_all_nav() {
        return http.get("/sys/menu/nav")
    },
    get_menu_list() {
        return http.get("/sys/menu/list")
    },
    save_menu_info(formData) {
        return http.post("/sys/menu", formData)
    },
    update_menu_info(formData) {
        return http.put("/sys/menu", formData)
    },
    delete_menu_info(id) {
        return http.delete(`/sys/menu/${id}`)
    }
})
export default data;
