import {reactive} from "vue";
import http from "@/utils/http";

const data = reactive({
    get_all_role(currentPage, sizePage) {
        return http.get(`/sys/role/${currentPage}/${sizePage}`)
    },
    get_permission_id(role_id) {
        return http.get(`/sys/role/permission/${role_id}`)
    },
    save_role_info(formData) {
        return http.post("/sys/role", formData)
    },
    get_all_menu() {
        return http.get("/sys/menu/role");
    },
    set_role_permission(role_id, idTableData) {
        return http.post(`/sys/role/permission/${role_id}`, idTableData)
    },
    delete_role_permission(role_id, idTableData) {
        return http.delete(`/sys/role/permission/${role_id}`, {
            data: idTableData
        })
    },
    delete_role_info(idTableData) {
        return http.delete("/sys/role", {
            data: idTableData
        })
    },
    update_role_info(formData) {
        return http.put("/sys/role", formData)
    }

})
export default data;