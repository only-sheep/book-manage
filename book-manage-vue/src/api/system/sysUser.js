import http from "../../utils/http"
import {reactive} from "vue"

const data = reactive({
    get_all_user(currentPage, sizePage) {
        return http.get(`/sys/user/${currentPage}/${sizePage}`)
    },
    get_user_info() {
        return http.get("/sys/user/self")
    },
    delete_user_info(idTableData) {
        return http.delete("/sys/user", {
            data: idTableData,
        })
    },
})
export default data;
