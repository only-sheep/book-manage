const state = {
    get UserToken() {
        return localStorage.getItem('jwt_token');
    },

    set UserToken(value) {
        localStorage.setItem('jwt_token', value);
    },

    set UserPermission(value) {
        localStorage.setItem('permissions', value);
    },
    get UserPermission() {
        return localStorage.getItem('permissions');
    },
};
const mutations = {
    // 登录
    LOGIN_IN(state, token) {
        state.UserToken = token;
    },

    // 登出
    LOGIN_OUT(state) {
        state.UserToken = "";
    },

    // 设置权限
    SET_PERMISSION(state, permission) {
        state.UserPermission = permission;
    },

    // 清楚权限
    CLEAR_PERMISSION(state) {
        state.UserPermission = null;
    },

    // 设置路由菜单
    SET_MENU(state, menu) {
        state.UserMenu = menu;
    }
};
const actions = {};
const getters = {};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}