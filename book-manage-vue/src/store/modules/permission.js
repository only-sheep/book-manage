import data from "@/api/system/sysMenu";
import {routes} from "@/router"

/**
 *
 * {
        meta: {
                    title: "书籍管理",
                    keepAlive: true
                },
                path: '/book',
                name: 'Book',
                component: () => import("../views/Book")
            },
 * @param menus
 */

function formatRouter(menus) {
    const menuArray = []
    if (menus && menus.length > 0) {
        // 循环菜单
        menus.forEach(menu => {
                // 每次循环声明对象，用于存储临时数据
                if (menu.type !== 2 && menu.type !== 3) {
                    const menuObj = {}
                    menuObj.path = menu.path;
                    menuObj.name = menu.name;
                    menuObj.meta = {
                        title: menu.title,
                        keepAlive: true,
                        icon: menu.icon,
                    };
                    if (menu.children && menu.children.length > 0) {
                        menuObj.redirect = menu.children[0].path;
                    }
                    if (menu.component) {
                        menuObj.component = require("@/views/" + menu.component + ".vue").default;
                    }
                    menuObj.children = []
                    if (menu.children && menu.children.length > 0) {
                        menu.children.forEach(me => {
                            if (me.type !== 3) {
                                const meObj = {}
                                meObj.path = me.path;
                                meObj.name = me.name;
                                meObj.meta = {
                                    title: me.title,
                                    keepAlive: true,
                                    icon: me.icon,
                                };
                                if (me.redirect) {
                                    meObj.redirect = me.redirect;
                                }
                                if (me.component) {
                                    meObj.component = require("@/views/" + me.component + ".vue").default;
                                }
                                menuObj.children.push(meObj);
                            }
                        })
                    }
                    menuArray.push(menuObj);
                }
            }
        )
        // console.log("menuArray:", menuArray)
        return menuArray;
    }
}

const state = {
    asynd_router: [], // 后端返回数据
    all_router: [] // 静态路由+后台返回路由

};
// 同步处理
const mutations = {
    SET_ROUTER(state, router) {
        const routers = {
            meta: {
                title: "图书管理",
                keepAlive: true
            },
            path: '/',         // uri访问地址
            name: "layout",
            component: () => import("@/views/Layout"),
            redirect: router[0].path,
            children: []
        }
        routers.children.push(router)
        state.asynd_router = router;

        state.all_router = routes.concat(routers)
    },
};
// 异步处理
const actions = {
    actionGetPermissionMenu(context, params) {
        return new Promise(((resolve, reject) => {
            data.get_all_nav().then(response => {
                const menuTree = formatRouter(response.data.data.nav);
                context.commit("SET_ROUTER", menuTree)
                resolve()
            })
        }))
    }
};
const getters = {};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}