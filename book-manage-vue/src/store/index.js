import {createStore} from 'vuex'
// import menu from "@/store/menu";
import permission from "@/store/modules/permission";
import account from "@/store/modules/account";

export default createStore({
    modules: {
        permission,
        account
    },
})


