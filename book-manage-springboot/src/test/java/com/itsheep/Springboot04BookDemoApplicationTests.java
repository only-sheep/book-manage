package com.itsheep;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itsheep.entity.Book;
import com.itsheep.mapper.BookMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Springboot04BookDemoApplicationTests {

	@Autowired
	private BookMapper bookMapper;
	@Test
	void contextLoads() {
		LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<Book>();
		lqw.lt(Book::getId,20);
		List<Book> bookList = bookMapper.selectList(lqw);
		System.out.println(bookList);
	}

	@Test
	void deleteTest(){

		int result = bookMapper.deleteById(42);
		System.out.println(result);

	}

	@Test
	void selectGoodTypeTest(){
		List<String> list = bookMapper.selectBookType();
		System.out.println(list);
	}


}
