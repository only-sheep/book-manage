package com.itsheep;

import com.itsheep.entity.sys.SysMenu;
import com.itsheep.mapper.sys.SysMenuMapper;
import com.itsheep.service.SysMenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SysMenuTest {

    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Autowired
    private SysMenuService sysMenuService;
    @Test
    public void testSelectMenuListByUserId(){
        List<SysMenu> sysMenuList = sysMenuMapper.selectMenuListByUserId(5);
        System.out.println(sysMenuList);
    }
    @Test
    public void testMenuTree(){
        for (SysMenu menu : sysMenuService.menuTree(1)) {
            System.out.println(menu);
        }

    }

}
