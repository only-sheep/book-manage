package com.itsheep;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itsheep.controller.BaseController;
import com.itsheep.entity.sys.NavMenu;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.mapper.sys.SysMenuMapper;
import com.itsheep.mapper.sys.SysRoleMapper;
import com.itsheep.mapper.sys.SysUserMapper;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class MapperTest extends BaseController {

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Test
    public void testUserMapper() {
        LambdaQueryWrapper<SysUser> lqw = new LambdaQueryWrapper<>();
        lqw.select(SysUser::getId, SysUser::getUsername, SysUser::getRealName, SysUser::getAvatar, SysUser::getPhoneNumber, SysUser::getAddress, SysUser::getRemark, SysUser::getStatus, SysUser::getCreateTime, SysUser::getUpdateBy, SysUser::getUpdateTime);
        List<SysUser> users = userMapper.selectList(lqw);
        System.out.println(users);
    }

    @Test
    public void TestBCryptPasswordEncoder() {
        val encode = passwordEncoder.encode("12345");
        System.out.println(encode);
        boolean matches = passwordEncoder.matches("12345", encode);
        System.out.println(matches);
    }

    @Test
    public void TestSelectNavListByUserId() {
        List<NavMenu> navMenus = sysMenuService.navTree(2);
        System.out.println(navMenus);

    }

    @Test
    public void TestSelectPermsByUserId() {
        List<String> permissionList = sysUserMapper.selectPermsByUserId(2);
        System.out.println(permissionList);

    }

    @Test
    public void TestselectNavListByUserId() {
        List<NavMenu> navMenus = sysMenuService.navTree(2);
        System.out.println(navMenus);

    }

    @Test
    public void TestSelectPermissionIdByRoleIdMapper() {
        List<Integer> list = sysRoleMapper.selectPermissionIdByRoleId(2);
        System.out.println(list);

    }

    @Test
    public void TestInsertRolesMenusPermission() {
        List<String> list = new ArrayList<>();
        list.add("18");
        list.add("1");
        list.add("4");
        list.add("2");
        list.add("3");
        list.add("19");
        list.add("20");
        list.add("22");
        list.add("21");
        list.add("17");
        Integer integer = sysRoleMapper.insertRolesMenusPermission(6, list);
        System.out.println(integer);
    }

    @Test
    public void TestDeleteRolesMenusPermission(){
        List<String> list = new ArrayList<>();
        list.add("18");
        list.add("1");
        list.add("4");
        list.add("2");
        list.add("3");
        list.add("19");
        list.add("20");
        list.add("22");
        list.add("21");
        list.add("17");
        Integer integer = sysRoleMapper.deleteRolesMenusPermission(6, list);
        System.out.println(integer);
    }
}
