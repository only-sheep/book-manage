package com.itsheep.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itsheep.entity.Book;
import com.itsheep.exception.BusinessException;
import com.itsheep.mapper.BookMapper;
import com.itsheep.service.BookService;
import com.itsheep.utils.controller.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {

    @Autowired
    private BookMapper bookMapper;
    @Override
    public List<String> getBookAllType() {
        try {
            return bookMapper.selectBookType();
        }catch (Exception e){
            throw new BusinessException(Code.GET_ERR,"书籍类型查询失败！");
        }
    }
}
