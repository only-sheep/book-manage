package com.itsheep.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itsheep.entity.sys.SysRole;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.mapper.sys.SysRoleMapper;
import com.itsheep.mapper.sys.SysUserMapper;
import com.itsheep.service.SysRoleService;
import com.itsheep.utils.controller.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    public HttpServletRequest request; //自动注入request

    @Override
    public boolean saveRole(SysRole sysRole) {
        int result = 0;
        try {
            Date date = new Date();
            sysRole.setCreateTime(date);
            SysUser sysUser = sysUserMapper.selectById((Integer) request.getAttribute("user_id"));
            sysRole.setCreateBy(sysUser.getUsername());
            result = sysRoleMapper.insert(sysRole);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "添加角色失败！");
        }

        return result > 0;

    }

    @Override
    public boolean updateRole(SysRole sysRole) {
        int result = 0;
        try {
            Date date = new Date();
            sysRole.setUpdateTime(date);
            SysUser sysUser = sysUserMapper.selectById((Integer) request.getAttribute("user_id"));
            sysRole.setUpdateBy(sysUser.getUsername());
            result = sysRoleMapper.updateById(sysRole);
        } catch (Exception e) {
            throw new BusinessException(Code.UPDATE_ERR, "修改角色失败！");
        }

        return result > 0;
    }

    @Override
    public List<Integer> getRolePermissionId(Integer role_id) {
        return sysRoleMapper.selectPermissionIdByRoleId(role_id);
    }

    @Override
    public boolean setPermissionToRole(Integer role_id, List<String> idList) {
        Integer result = 0;

        try {
            result = sysRoleMapper.insertRolesMenusPermission(role_id, idList);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "角色权限设置失败！");
        }
        // TODO 要改善的地方
        return result >= 0;


    }

    @Override
    public boolean removeRoleMenuPermission(Integer role_id, List<String> idList) {
        Integer result = 0;
        try {
            result = sysRoleMapper.deleteRolesMenusPermission(role_id, idList);
        } catch (Exception e) {
            throw new BusinessException(Code.DELETE_ERR, "角色权限删除失败！");
        }
        return result >= 0;
    }
}
