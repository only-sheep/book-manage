package com.itsheep.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itsheep.entity.BookLendRecord;
import com.itsheep.exception.BusinessException;
import com.itsheep.mapper.BookLendRecordMapper;
import com.itsheep.service.BookLendRecordService;
import com.itsheep.utils.controller.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BookLendRecordServiceImpl extends ServiceImpl<BookLendRecordMapper, BookLendRecord> implements BookLendRecordService {

    @Autowired
    private BookLendRecordMapper bookLendRecordMapper;

    // 未使用，学生端使用该接口借阅书籍
    @Override
    public boolean saveInfo(BookLendRecord bookLendRecord) {
        bookLendRecord.setIsDeleted(0);
        bookLendRecord.setBorrowStatus(0);

        Date date = new Date();
        bookLendRecord.setBorrowTime(date);
        int result;
        try {
            result = bookLendRecordMapper.insert(bookLendRecord);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "借阅信息添加失败！");
        }
        return result > 0;
    }
}
