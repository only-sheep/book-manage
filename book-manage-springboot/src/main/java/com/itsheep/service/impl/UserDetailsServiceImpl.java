package com.itsheep.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itsheep.entity.sys.SysMenu;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.mapper.sys.SysMenuMapper;
import com.itsheep.mapper.sys.SysUserMapper;
import com.itsheep.utils.LoginUser;
import com.itsheep.utils.controller.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名查询用户信息
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        // #TODO 要做用户名唯一性判断
        wrapper.eq(SysUser::getUsername, username);
        // 筛选出状态异常的用户
        wrapper.eq(SysUser::getStatus, 0);
        SysUser user = userMapper.selectOne(wrapper);
        //如果查询不到数据就通过抛出异常来给出提示
        if (Objects.isNull(user)) {
            throw new BusinessException(Code.LOGIN_ERR, "用户名或密码错误");
        }
        // 根据用户查询权限信息 添加到LoginUser中
        List<String> sysMenuList = userMapper.selectPermsByUserId(user.getId());
        System.out.println(sysMenuList);
        //封装成UserDetails对象返回
        return new LoginUser(user, sysMenuList);
    }
}
