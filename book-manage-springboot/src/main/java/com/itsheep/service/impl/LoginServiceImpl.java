package com.itsheep.service.impl;

import com.itsheep.entity.sys.SysUser;
import com.itsheep.mapper.sys.SysUserMapper;
import com.itsheep.service.LoginService;
import com.itsheep.utils.LoginUser;
import com.itsheep.utils.RedisCache;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.JwtUtil;
import com.itsheep.utils.controller.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public Result login(SysUser user) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        if (Objects.isNull(authenticate)) {
            throw new RuntimeException("用户名或密码错误");
        }
        //使用userid生成token
        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        String userId = loginUser.getUser().getId().toString();
        String jwt = JwtUtil.createJWT(userId);
        //authenticate存入redis
        redisCache.setCacheObject("login:" + userId, loginUser);
        //把token响应给前端
        HashMap<String, String> map = new HashMap<>();
        map.put("token", jwt);
        return new Result(Code.LOGIN_OK, map, "登陆成功");
    }

    @Override
    public Result logout() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        Integer userid = loginUser.getUser().getId();
        redisCache.deleteObject("login:" + userid);
        return new Result(Code.LOGOUT_OK, null, "退出成功");
    }

    @Override
    public Result getPermission(Integer user_id) {
        List<String> permissionList = sysUserMapper.selectPermsByUserId(user_id);
        return new Result(Code.GET_OK, permissionList, "获取成功");
    }

    @Override
    public List<String> getPermissions(Integer user_id) {
        return sysUserMapper.selectPermsByUserId(user_id);
    }
}

