package com.itsheep.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.mapper.sys.SysUserMapper;
import com.itsheep.service.SysUserService;
import com.itsheep.utils.controller.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public SysUser login(SysUser user) {

        String username = user.getUsername();
        String password = user.getPassword();
        QueryWrapper<SysUser> qw = new QueryWrapper<>();
        if (username != null && !username.equals("") && password != null && !password.equals("")) {
            qw.eq("username", username).eq("password", password);
            return sysUserMapper.selectOne(qw);
        }
        throw new BusinessException(Code.LOGIN_ERR, "账号或密码不能为空！");

    }

    @Override
    public boolean register(SysUser user) {
        user.setIs_deleted(0);
        user.setStatus(0);
        String password = user.getPassword();
        String encodePassword = passwordEncoder.encode(password);
        user.setPassword(encodePassword);
        int result = 0;
        try {
            Date date = new Date();
            user.setCreateTime(date);
            result = sysUserMapper.insert(user);
        } catch (Exception e) {
            throw new BusinessException(Code.REGISTER_ERR, "注册失败！");
        }
        return result > 0;
    }
}
