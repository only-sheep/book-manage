package com.itsheep.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itsheep.entity.sys.NavMenu;
import com.itsheep.entity.sys.SysMenu;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.mapper.sys.SysMenuMapper;
import com.itsheep.mapper.sys.SysUserMapper;
import com.itsheep.service.SysMenuService;
import com.itsheep.utils.controller.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    public HttpServletRequest request; //自动注入request

    @Override
    public List<SysMenu> menuTree(Integer userId) {
        // 获取所有菜单权限
        List<SysMenu> sysMenuList = sysMenuMapper.selectMenuListByUserId(userId);
        List<SysMenu> menuList = new ArrayList<>();
        for (SysMenu menu : sysMenuList) {
            getChildrenList(menu, sysMenuList);
        }
        for (SysMenu menu : sysMenuList) {
            if (menu.getParentId() == -1) {
                menuList.add(menu);
            }
        }
        return menuList;
    }

    @Override
    public List<SysMenu> menuTreeRole(Integer userId) {
        // 获取所有菜单权限
        List<SysMenu> sysMenuList = sysMenuMapper.selectMenuListByUserIdRole(userId);
        List<SysMenu> menuList = new ArrayList<>();
        for (SysMenu menu : sysMenuList) {
            getChildrenList(menu, sysMenuList);
        }
        for (SysMenu menu : sysMenuList) {
            if (menu.getParentId() == -1) {
                menuList.add(menu);
            }
        }
        return menuList;
    }

    @Override
    public List<NavMenu> navTree(Integer userId) {

        // 获取所有菜单
        List<NavMenu> sysMenuList = sysMenuMapper.selectNavListByUserId(userId);
        List<NavMenu> navList = new ArrayList<>();
        for (NavMenu nav : sysMenuList) {
            getChildrenList(nav, sysMenuList);
        }
        for (NavMenu nav : sysMenuList) {
            if (nav.getParentId() == -1) {
                navList.add(nav);
            }
        }
        return navList;
    }

    @Override
    public boolean saveMenu(SysMenu sysMenu) {
        int result = 0;
        try {
            Date date = new Date();
            sysMenu.setCreateTime(date);
            SysUser sysUser = sysUserMapper.selectById((Integer) request.getAttribute("user_id"));
            sysMenu.setCreateBy(sysUser.getUsername());
            result = sysMenuMapper.insert(sysMenu);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "添加菜单失败！");
        }

        return result > 0;

    }

    @Override
    public boolean updateMenu(SysMenu sysMenu) {
        int result = 0;
        try {
            Date date = new Date();
            sysMenu.setUpdateTime(date);
            SysUser sysUser = sysUserMapper.selectById((Integer) request.getAttribute("user_id"));
            sysMenu.setUpdateBy(sysUser.getUsername());
            result = sysMenuMapper.updateById(sysMenu);
        } catch (Exception e) {
            throw new BusinessException(Code.UPDATE_ERR, "修改菜单失败！");
        }

        return result > 0;
    }

    private NavMenu getChildrenList(NavMenu sysMenu, List<NavMenu> sysNavList) {
        for (NavMenu nav : sysNavList) {
            if (nav.getParentId() != -1 && nav.getParentId().equals(sysMenu.getId())) {
                sysMenu.getChildren().add(getChildrenList(nav, sysNavList));
            }
        }
        return sysMenu;
    }

    public SysMenu getChildrenList(SysMenu sysMenu, List<SysMenu> sysMenuList) {
        for (SysMenu menu : sysMenuList) {
            if (menu.getParentId() != -1 && menu.getParentId().equals(sysMenu.getId())) {
                sysMenu.getChildren().add(getChildrenList(menu, sysMenuList));
            }
        }
        return sysMenu;
    }

}
