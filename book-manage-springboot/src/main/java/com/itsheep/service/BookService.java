package com.itsheep.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itsheep.entity.Book;

import java.util.List;

public interface BookService extends IService<Book> {
    List<String> getBookAllType();
}
