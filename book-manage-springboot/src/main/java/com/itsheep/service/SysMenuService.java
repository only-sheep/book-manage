package com.itsheep.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itsheep.entity.sys.NavMenu;
import com.itsheep.entity.sys.SysMenu;

import java.util.List;

public interface SysMenuService extends IService<SysMenu> {

    public List<SysMenu> menuTree(Integer userId);
    public List<SysMenu> menuTreeRole(Integer userId);

    public List<NavMenu> navTree(Integer userId);

    public boolean saveMenu(SysMenu sysMenu);

    public boolean updateMenu(SysMenu sysMenu);

}
