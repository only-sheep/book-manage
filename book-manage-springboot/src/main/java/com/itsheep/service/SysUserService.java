package com.itsheep.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.utils.controller.Result;

public interface SysUserService extends IService<SysUser> {
    public SysUser login(SysUser user);

    public boolean register(SysUser user);

}
