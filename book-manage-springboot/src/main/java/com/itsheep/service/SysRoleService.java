package com.itsheep.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itsheep.entity.sys.SysRole;
import org.apache.ibatis.annotations.Param;


import java.util.Date;
import java.util.List;

public interface SysRoleService extends IService<SysRole> {

    public boolean saveRole(SysRole sysRole);

    boolean updateRole(SysRole sysRole);

    List<Integer> getRolePermissionId(Integer role_id);

    boolean setPermissionToRole(Integer role_id,List<String> idList);

    boolean removeRoleMenuPermission(Integer role_id, List<String> idList);
}
