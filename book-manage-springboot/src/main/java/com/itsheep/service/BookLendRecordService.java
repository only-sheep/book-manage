package com.itsheep.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itsheep.entity.BookLendRecord;
import com.itsheep.entity.sys.SysUser;

public interface BookLendRecordService extends IService<BookLendRecord> {
    public boolean saveInfo(BookLendRecord bookLendRecord);
}
