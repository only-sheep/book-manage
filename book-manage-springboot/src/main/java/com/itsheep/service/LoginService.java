package com.itsheep.service;

import com.itsheep.entity.sys.SysUser;
import com.itsheep.utils.controller.Result;

import java.util.List;

public interface LoginService {
    public Result login(SysUser user);

    public Result logout();

    public Result getPermission(Integer user_id);

    public List<String> getPermissions(Integer user_id);
}
