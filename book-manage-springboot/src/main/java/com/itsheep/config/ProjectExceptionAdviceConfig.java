package com.itsheep.config;

import com.itsheep.exception.BusinessException;
import com.itsheep.exception.SystemException;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


@RestControllerAdvice
public class ProjectExceptionAdviceConfig {
    @ExceptionHandler(SystemException.class)
    public Result doSystemException(SystemException e) {
        // 记录日志

        // 发消息给运维人员

        // 发邮件给开发人员
        return new Result(e.getCode(), false, e.getMessage());
    }

    @ExceptionHandler(BusinessException.class)
    public Result doBusinessException(BusinessException e) {
        return new Result(e.getCode(), false, e.getMessage());
    }

//    /**
//     * 请求方式不支持
//     */
//    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
//    public Result handleException(HttpRequestMethodNotSupportedException e) {
//        return new Result(Code.METHOD_ERR,null, "不支持' " + e.getMethod() + "'请求");
//    }
//
//    /**
//     * 拦截未知的运行时异常
//     */
//    @ExceptionHandler(RuntimeException.class)
//    public Result notFount(RuntimeException e) {
//        return new Result(Code.SYSTEM_ERR, null, "运行时异常:" + e.getMessage());
//    }
//
    /**
     * 系统异常
     */
//    @ExceptionHandler(Exception.class)
//    public Result handleException(Exception e) {
//        System.out.println("服务器错误-->" + e);
//        return new Result(Code.SYSTEM_UNKNOW_ERR, null, "服务器错误，请联系管理员");
//    }

    /**
     * 校验异常
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Result exceptionHandler(MethodArgumentNotValidException customException) {
        String errorMessage = customException.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return new Result(Code.VALID_ERR, false, errorMessage);
    }

//    /**
//     * 校验异常
//     */
//    @ExceptionHandler(value = BindException.class)
//    public Result validationExceptionHandler(BindException e) {
//        BindingResult bindingResult = e.getBindingResult();
//        String errorMesssage = "";
//        for (FieldError fieldError : bindingResult.getFieldErrors()) {
//            errorMesssage += fieldError.getDefaultMessage() + "!";
//        }
//        return new Result(Code.VALID_ERR, null, errorMesssage);
//    }
//
//    /**
//     * 校验异常
//     */
//    @ExceptionHandler(value = ConstraintViolationException.class)
//    public Result ConstraintViolationExceptionHandler(ConstraintViolationException ex) {
//        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
//        Iterator<ConstraintViolation<?>> iterator = constraintViolations.iterator();
//        List<String> msgList = new ArrayList<>();
//        while (iterator.hasNext()) {
//            ConstraintViolation<?> cvl = iterator.next();
//            msgList.add(cvl.getMessageTemplate());
//        }
//        return new Result(Code.VALID_ERR, null, String.join(",", msgList));
//    }
//
//    /**
//     * 业务异常
//     */
//    @ExceptionHandler(BusinessException.class)
//    public Result businessException(BusinessException e) {
//
//        return new Result(Code.SERVICE_ERR,null, e.getMessage());
//    }

    /**
     * 演示模式异常
     */
//    @ExceptionHandler(DemoModeException.class)
//    public Result demoModeException(DemoModeException e) {
//        return AjaxResult.error("演示模式，不允许操作");
//    }


}
