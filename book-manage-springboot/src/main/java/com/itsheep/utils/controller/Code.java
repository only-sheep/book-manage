package com.itsheep.utils.controller;

public class Code {
    public static final Integer SAVE_OK = 20011;
    public static final Integer DELETE_OK = 20021;
    public static final Integer UPDATE_OK = 20031;
    public static final Integer GET_OK = 20041;

    public static final Integer SAVE_ERR = 20010;
    public static final Integer DELETE_ERR = 20020;
    public static final Integer UPDATE_ERR = 20030;
    public static final Integer GET_ERR = 20040;


    public static final Integer SYSTEM_ERR = 50001;
    public static final Integer SYSTEM_TIMEOUT_ERR = 50002;
    public static final Integer SYSTEM_UNKNOW_ERR = 59999;
    public static final Integer BUSINESS_ERR = 60001;

    public static final Integer LOGIN_OK = 40001;
    public static final Integer REGISTER_OK = 60001;

    public static final Integer LOGIN_ERR = 40000;
    public static final Integer REGISTER_ERR = 60000;


    public static final Integer SERVICE_ERR = 10000;
    public static final Integer VALID_ERR = 30000;
    public static final Integer METHOD_ERR = 70000;
    public static final Integer LOGOUT_OK = 40001;
}
