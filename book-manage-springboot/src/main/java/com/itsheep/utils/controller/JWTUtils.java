package com.itsheep.utils.controller;

//import com.alibaba.druid.util.StringUtils;
//import com.auth0.jwt.JWT;
//import com.auth0.jwt.JWTCreator;
//import com.auth0.jwt.JWTVerifier;
//import com.auth0.jwt.algorithms.Algorithm;
//import com.auth0.jwt.interfaces.DecodedJWT;
import com.itsheep.entity.sys.SysUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JWTUtils {

    @Value("${jwt.secret_key}")
    private String secretKey;

    private static long expire = 604800;

    private static String secret;

    // 将静态资源加载到静态类的成员静态变量中
    // 注解@PostConstruct会在servlet服务器加载的时候加载一次
    @PostConstruct
    public void init() {
        secret = this.secretKey;
    }

    /**
     * 获取token
     *
     * @param SysUser user
     * @return token
     */
    public static String createToken(SysUser user) {
        Date date = new Date();
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setSubject(user.getUsername())
                .setId(user.getId().toString())
                .setIssuedAt(date)
                .setExpiration(new Date(date.getTime() + 1000 * expire))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public static Claims parserToken(String jwt) {
        try {
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isExpire(Claims claims) {
        return claims.getExpiration().before(new Date());
    }
    /**
     * 验证token合法性 成功返回token
     *
     * @param token token
     * @return token
     * @throws Exception e
     */
//    public static DecodedJWT verify(String token) throws Exception {
//        if (StringUtils.isEmpty(token)) {
//            throw new Exception("token不能为空");
//        }
//
//        JWTVerifier build = JWT.require(Algorithm.HMAC256(secret)).build();
//        return build.verify(token);
//    }
}
