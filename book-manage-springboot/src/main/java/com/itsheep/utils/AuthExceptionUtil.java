package com.itsheep.utils;

import com.itsheep.utils.controller.Result;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.csrf.CsrfException;


public class AuthExceptionUtil {
    public static Result getErrMsgByExceptionType(AuthenticationException e) {
        if (e instanceof LockedException) {
            return new Result(1100,null, "账户被锁定，请联系管理员!");
        } else if (e instanceof CredentialsExpiredException) {
            return new Result(1105,null, "用户名或者密码输入错误!");
        }else if (e instanceof InsufficientAuthenticationException) {
            return new Result(403,null, "权限不足请重新登录!");
        } else if (e instanceof AccountExpiredException) {
            return new Result(1101, null, "账户过期，请联系管理员!");
        } else if (e instanceof DisabledException) {
            return new Result(1102,null,  "账户被禁用，请联系管理员!");
        } else if (e instanceof BadCredentialsException) {
            return new Result(1105,null,  "用户名或者密码输入错误!");
        }else if (e instanceof AuthenticationServiceException) {
            return new Result(1106, null, "认证失败，请重试!");
        }

        return new Result(1200,null,  e.getMessage());
    }
    public static Result getErrMsgByExceptionType(AccessDeniedException e) {
        if (e instanceof CsrfException) {
            return new Result(-1001,null,  "非法访问跨域请求异常!");
        } else if (e instanceof CsrfException) {
            return new Result(-1002,null, "非法访问跨域请求异常!");
        } else if (e instanceof AuthorizationServiceException) {
            return new Result(1101, null, "认证服务异常请重试!");
        }else if (e instanceof AccessDeniedException) {
            return new Result(4003, null, "权限不足不允许访问!");
        }

        return new Result(1200,null,  e.getMessage());
    }
}