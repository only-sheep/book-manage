package com.itsheep.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@TableName("tb_roles_menus")
public class SysMenuRole {
    private Integer id;
    @NotNull
    @TableField("role_id")
    private Integer roleId;
    @NotNull
    @TableField("menu_id")
    private Integer menuId;
}
