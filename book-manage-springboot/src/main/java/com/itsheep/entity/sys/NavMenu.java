package com.itsheep.entity.sys;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NavMenu {
    private Integer id;
    private Integer parentId;
    private String name;
    private String title;
    private Integer enable;
    private Integer type;
    private String icon;
    private String path;
    private String component;
    private Integer sort;
    private Set<NavMenu> children = new HashSet<>();
}
