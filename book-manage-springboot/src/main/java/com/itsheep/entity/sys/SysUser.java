package com.itsheep.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_user")
public class SysUser {
    //    @NotNull(message = "id不能为空")
    private Integer id;

    @NotBlank(message = "用户名不能为空")
    @Length(min = 5, max = 8, message = "用户名长度最小不能低于5位，最大不能高于8位")
    private String username;

    //    @TableField(select = false)
    @NotBlank(message = "密码不能为空")
    @Length(min = 10, max = 15, message = "密码长度最小不能低于10位，最大不能高于15位")
    private String password;

    @TableField(value = "real_name")
    private String realName;

    @NotNull(message = "性别不能为空！")
    private Integer gender;

    private String avatar;

    @TableField(value = "phone_number")
    @NotBlank(message = "电话号码不能为空")
    @Pattern(regexp = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$", message = "电话号码格式不正确")
    private String phoneNumber;

    private String address;

    private String remark;

    private Integer status;
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @TableField(value = "update_by")
    private String updateBy;
    @TableField(value = "update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @TableLogic(value = "0", delval = "1")
    private Integer is_deleted;
}


