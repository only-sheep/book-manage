package com.itsheep.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_menu")
public class SysMenu {
    private Integer id;
    @TableField(value = "parent_id")
    @NotNull(message = "父菜单不能为空！")
    private Integer parentId;
    private String name;
    @NotBlank(message = "菜单标题不能为空！")
    private String title;
    private String component;
    private Integer sort;
    private String icon;
    private Integer type;
    private String path;
    private Integer enable;
    private String permission;
    @TableField(value = "create_by")
    private String createBy;
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @TableField(value = "update_by")
    private String updateBy;
    @TableField(value = "update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @TableField(exist = false)
    private Set<SysMenu> children = new HashSet<>();
    @TableLogic(value = "0", delval = "1")
    private Integer is_deleted;
}
