package com.itsheep.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@TableName("tb_role")
@NoArgsConstructor
@AllArgsConstructor
public class SysRole {
    private Integer id;
    @TableField("role_name")
    @NotNull
    private String roleName;
    private Integer level;
    private String description;
    @TableField("data_scope")
    private String dataScope;
    private Integer status;
    @TableField(value = "create_by")
    private String createBy;
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @TableField(value = "update_by")
    private String updateBy;
    @TableField(value = "update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @TableLogic(value = "0", delval = "1")
    private Integer is_deleted;
}
