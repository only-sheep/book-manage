package com.itsheep.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@TableName("tb_roles_users")
public class SysUserRole {
    private Integer id;
    @NotNull
    private Integer userId;
    @NotNull
    private Integer roleId;
}
