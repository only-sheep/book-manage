package com.itsheep.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private Integer id;
    private String type;
    private String name;
    private Integer count;
    private BigDecimal price;
    private String description;
    @TableLogic(value = "0", delval = "1")
    private Integer is_deleted;
}
