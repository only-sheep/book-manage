package com.itsheep.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_book_record")
public class BookLendRecord {
    private Integer id;
    @TableField(value = "borrow_id")
    private String borrowId;
    @TableField(value = "borrow_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date borrowTime;
    @TableField(value = "back_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date backTime;
    private String remark;
    @TableField(value = "book_id")
    private Integer bookId;
    @TableField(exist = false)
    private String username;
    @TableField(exist = false)
    private String bookName;
    @TableField(value = "user_id")
    private Integer userId;
    @TableField(value = "borrow_status")
    private Integer borrowStatus;
    @TableLogic(value = "0", delval = "1")
    @TableField(value = "is_deleted")
    private Integer isDeleted;
}
