package com.itsheep.entity;

import com.itsheep.entity.sys.SysUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 注册实体类
 */
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUser extends SysUser {
    private String rePassword;

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }
}
