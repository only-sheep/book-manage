package com.itsheep.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itsheep.entity.Book;
import com.itsheep.entity.BookLendRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BookLendRecordMapper extends BaseMapper<BookLendRecord> {
}
