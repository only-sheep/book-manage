package com.itsheep.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itsheep.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookMapper extends BaseMapper<Book> {
    @Select("select distinct(type) from tb_book where is_deleted=0")
    List<String> selectBookType();
}
