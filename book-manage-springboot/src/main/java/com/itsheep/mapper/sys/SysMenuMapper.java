package com.itsheep.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itsheep.entity.sys.NavMenu;
import com.itsheep.entity.sys.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {
    public List<SysMenu> selectMenuListByUserId(@Param("user_id") Integer userId);

    public List<SysMenu> selectMenuListByUserIdRole(@Param("user_id") Integer userId);

    public List<NavMenu> selectNavListByUserId(@Param("user_id") Integer userId);

}
