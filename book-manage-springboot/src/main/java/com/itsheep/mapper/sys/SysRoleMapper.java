package com.itsheep.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itsheep.entity.sys.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<Integer> selectPermissionIdByRoleId(@Param("role_id") Integer role_id);

    Integer insertRolesMenusPermission(@Param("role_id") Integer role_id, @Param("permissionList") List<String> idList);

    Integer deleteRolesMenusPermission(@Param("role_id") Integer role_id, @Param("permissionList") List<String> idList);
}
