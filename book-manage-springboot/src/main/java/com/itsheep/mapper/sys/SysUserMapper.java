package com.itsheep.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itsheep.entity.sys.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
//    public List<String> getUserPermissionById(@Param("user_id") Integer userId);

    public List<String> selectPermsByUserId(@Param("user_id") Integer id);
}
