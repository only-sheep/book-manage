package com.itsheep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot04BookDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springboot04BookDemoApplication.class, args);
	}

}
