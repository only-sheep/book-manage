package com.itsheep.controller;

import com.itsheep.entity.RegisterUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.service.SysUserService;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping("/register")
public class RegisterController {
    @Autowired
    private SysUserService sysUserService;

    @PostMapping
    public Result register(@Valid @RequestBody RegisterUser user) {
        if (!Objects.equals(user.getRePassword(), user.getPassword())) {
            throw new BusinessException(Code.REGISTER_ERR, "两次密码不一致！");
        }
        boolean bool = sysUserService.register(user);
        return new Result(Code.REGISTER_OK, bool, "注册成功！");
    }
}
