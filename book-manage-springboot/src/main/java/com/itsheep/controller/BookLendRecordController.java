package com.itsheep.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itsheep.entity.Book;
import com.itsheep.entity.BookLendRecord;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/bookRecord")
public class BookLendRecordController extends BaseController<BookLendRecord> {
    @GetMapping("{currentPage}/{sizePage}")
    @PreAuthorize("hasAuthority('lend:info:list')")
    public Result bookList(@PathVariable Integer currentPage, @PathVariable Integer sizePage) {
        //1 创建IPage分页对象,设置分页参数,1为当前页码，3为每页显示的记录数
        IPage<BookLendRecord> page = new Page<>(currentPage, sizePage);
        LambdaQueryWrapper<BookLendRecord> lqw = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<SysUser> userLqw = new LambdaQueryWrapper<>();
        userLqw.select(SysUser::getUsername);
        IPage<BookLendRecord> bookLendRecordIPage = bookLendRecordService.page(page, lqw);

        for (BookLendRecord bookLendRecord : bookLendRecordIPage.getRecords()) {
            Integer userId = bookLendRecord.getUserId();
            Integer bookId = bookLendRecord.getBookId();
            Book book = bookService.getById(bookId);
            SysUser sysUser = sysUserService.getById(userId);
            bookLendRecord.setUsername(sysUser != null ? sysUser.getUsername() : "该用户已注销");
            bookLendRecord.setBookName(book != null ? book.getName() : "该书籍已注销");
        }

        return new Result(Code.GET_OK, bookLendRecordIPage, "查询成功！");
    }

    @PostMapping
    @PreAuthorize("hasAuthority('lend:info:add')")
    public Result save(@RequestBody BookLendRecord bookLendRecord) {
        boolean result = false;
        try {
            result = bookLendRecordService.save(bookLendRecord);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "借阅信息保存失败！");
        }
        return new Result(result ? Code.SAVE_OK : Code.SAVE_ERR, result, result ? "借阅信息添加成功！" : "借阅信息添加失败！");
    }

    @PutMapping
    @PreAuthorize("hasAuthority('lend:info:edit')")
    public Result update(@RequestBody BookLendRecord bookLendRecord) {
        boolean result = false;
        try {
            result = bookLendRecordService.updateById(bookLendRecord);
        } catch (Exception e) {
            throw new BusinessException(Code.UPDATE_ERR, "借阅信息修改失败！");
        }

        return new Result(result ? Code.UPDATE_OK : Code.UPDATE_ERR, result, result ? "借阅信息修改成功！" : "借阅信息修改失败！");
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('lend:info:del')")
    public Result delete(@PathVariable Integer id) {
        boolean result = false;
        try {
            result = bookLendRecordService.removeById(id);
        } catch (Exception e) {
            throw new BusinessException(Code.DELETE_ERR, "借阅信息天删除失败！");
        }

        return new Result(result ? Code.DELETE_OK : Code.DELETE_ERR, result, result ? "借阅信息天删除成功！" : "借阅信息删除失败！");
    }


}
