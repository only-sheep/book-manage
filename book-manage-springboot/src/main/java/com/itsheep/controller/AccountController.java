package com.itsheep.controller;

import com.itsheep.entity.sys.SysUser;
import com.itsheep.service.LoginService;
import com.itsheep.service.SysUserService;
import com.itsheep.utils.controller.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class AccountController extends BaseController {

    @Autowired
    private LoginService loginService;


    // 登录
    @PostMapping("/login")
    public Result login(@RequestBody SysUser user) {
        return loginService.login(user);
    }

    // 登出
    @GetMapping("/logout")
    public Result logout() {
        return loginService.logout();
    }

    // 获取用户权限
    @GetMapping("/permission")
    public Result permission(){
        Integer user_id = (Integer)request.getAttribute("user_id");
        return loginService.getPermission(user_id);
    }

}
