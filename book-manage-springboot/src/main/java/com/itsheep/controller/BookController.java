package com.itsheep.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itsheep.entity.Book;
import com.itsheep.exception.BusinessException;
import com.itsheep.service.BookService;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController extends BaseController<Book> {

    @GetMapping("{currentPage}/{sizePage}")
    @PreAuthorize("hasAuthority('book:info:list')")
    public Result bookList(@PathVariable Integer currentPage, @PathVariable Integer sizePage, @RequestParam("input_search_key") String name, @RequestParam("select_search_key") String type) {
        //1 创建IPage分页对象,设置分页参数,1为当前页码，3为每页显示的记录数
        IPage<Book> page = new Page<>(currentPage, sizePage);
        LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<>();
        lqw.like(null != name, Book::getName, name).eq(null != type && !"".equals(type), Book::getType, type);

        IPage<Book> bookIPage = bookService.page(page, lqw);

        return new Result(Code.GET_OK, bookIPage, "查询成功！");
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('book:info:list')")
    public Result book(@PathVariable Integer id) {
        Book book = null;
        try {
            book = bookService.getById(id);
        } catch (Exception e) {
            throw new BusinessException(Code.GET_ERR, "查询失败！");
        }
        return new Result(Code.GET_OK, book, "查询成功！");
    }

    @PutMapping
    @PreAuthorize("hasAuthority('book:info:edit')")
    public Result update(@RequestBody Book book) {
        boolean bool = false;
        try {
            bool = bookService.updateById(book);
        } catch (Exception e) {
            throw new BusinessException(Code.UPDATE_ERR, "修改失败！");
        }
        return new Result(Code.UPDATE_OK, bool, "修改成功！");
    }

    @PostMapping
    @PreAuthorize("hasAuthority('book:info:add')")
    public Result save(@RequestBody Book book) {
        boolean bool = false;
        try {
            bool = bookService.save(book);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "保存失败！");
        }
        return new Result(Code.SAVE_OK, bool, "保存成功！");
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('book:info:del')")
    public Result delete(@PathVariable Integer id) {
        boolean bool = false;
        try {
            bool = bookService.removeById(id);
        } catch (Exception e) {
            throw new BusinessException(Code.DELETE_ERR, "删除失败！");
        }
        return new Result(Code.DELETE_OK, bool, "删除成功！");
    }

    @GetMapping("/type")
    @PreAuthorize("hasAuthority('book:info:list')")
    public Result getType() {
        List<String> bookTypeList = null;
        try {
            bookTypeList = bookService.getBookAllType();
        } catch (Exception e) {
            throw new BusinessException(Code.GET_ERR, "查询失败！");
        }
        return new Result(Code.GET_OK, bookTypeList, "查询成功！");
    }
}
