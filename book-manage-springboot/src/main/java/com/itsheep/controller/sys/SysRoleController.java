package com.itsheep.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itsheep.controller.BaseController;
import com.itsheep.entity.sys.SysRole;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends BaseController<SysRole> {

    @GetMapping("{currentPage}/{sizePage}")
    @PreAuthorize("hasAuthority('sys:role:list')")
    public Result roleList(@PathVariable Integer currentPage, @PathVariable Integer sizePage) {
        //1 创建IPage分页对象,设置分页参数,1为当前页码，3为每页显示的记录数
        IPage<SysRole> page = new Page<>(currentPage, sizePage);
        LambdaQueryWrapper<SysRole> lqw = new LambdaQueryWrapper<>();
        // 做查询用的接口
//        lqw.like(null != name, Book::getName, name).eq(null != type && !"".equals(type), Book::getType, type);

        // 排序
        lqw.orderBy(true, true, SysRole::getLevel);
        IPage<SysRole> bookIPage = sysRoleService.page(page, lqw);

        return new Result(Code.GET_OK, bookIPage, "用户信息获取成功！");
    }

    // 获取正常使用的菜单的信息
    @GetMapping("/permission/{id}")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result permissionList(@PathVariable("id") Integer role_id) {

        List<Integer> permissionIds = sysRoleService.getRolePermissionId(role_id);

        return new Result(Code.GET_OK, permissionIds, "权限id获取成功!");
    }

    @PostMapping
    @PreAuthorize("hasAuthority('sys:role:add')")
    public Result roleAdd(@RequestBody SysRole sysRole) {
        boolean result;
        try {
            result = sysRoleService.saveRole(sysRole);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "添加角色失败！");
        }
        return new Result(result ? Code.SAVE_OK : Code.SAVE_ERR, result, result ? "添加角色成功！" : "添加角色失败！");
    }

    @DeleteMapping
    @PreAuthorize("hasAuthority('sys:role:del')")
    public Result roleDel(@RequestBody List<String> idList) {
        boolean result;
        try {
            result = sysRoleService.removeByIds(idList);
        } catch (Exception e) {
            throw new BusinessException(Code.DELETE_ERR, "删除角色失败！");
        }
        return new Result(result ? Code.DELETE_OK : Code.DELETE_ERR, result, result ? "删除角色成功！" : "删除角色失败！");
    }

    // 删除角色分配的权限
    @DeleteMapping("/permission/{id}")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result roleMenuDel(@PathVariable("id") Integer role_id, @RequestBody List<String> idList) {
        boolean result = false;
        try {
            result = sysRoleService.removeRoleMenuPermission(role_id, idList);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "删除角色权限失败！");
        }
        return new Result(result ? Code.DELETE_OK : Code.DELETE_ERR, result, result ? "删除角色权限成功！" : "删除角色权限失败！");
    }

    @PutMapping
    @PreAuthorize("hasAuthority('sys:role:edit')")
    public Result roleEdit(@RequestBody SysRole sysRole) {
        boolean result;
        try {
            result = sysRoleService.updateRole(sysRole);
        } catch (Exception e) {
            throw new BusinessException(Code.UPDATE_ERR, "修改角色失败！");
        }

        return new Result(result ? Code.UPDATE_OK : Code.UPDATE_ERR, result, result ? "修改角色成功！" : "修改角色失败！");

    }

    @PostMapping("/permission/{id}")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result rolePermission(@PathVariable("id") Integer role_id, @RequestBody List<String> menu_ids) {
        boolean result = false;
        if (menu_ids == null || menu_ids.size() == 0) {
            return new Result(Code.SAVE_OK, true, "角色权限置空成功！");
        }
        try {
            result = sysRoleService.setPermissionToRole(role_id, menu_ids);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "角色权限设置失败！");
        }
        return new Result(result ? Code.SAVE_OK : Code.SAVE_ERR, result, result ? "角色权限设置成功！" : "角色权限设置失败！");
    }

}
