package com.itsheep.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itsheep.controller.BaseController;
import com.itsheep.entity.Book;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.service.SysUserService;
import com.itsheep.utils.LoginUser;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseController<SysUser> {

    @GetMapping("{currentPage}/{sizePage}")
    @PreAuthorize("hasAuthority('sys:user:list')")
    public Result userList(@PathVariable Integer currentPage, @PathVariable Integer sizePage) {
        //1 创建IPage分页对象,设置分页参数,1为当前页码，3为每页显示的记录数
        IPage<SysUser> page = new Page<>(currentPage, sizePage);
        LambdaQueryWrapper<SysUser> lqw = new LambdaQueryWrapper<>();
        lqw.select(SysUser::getId, SysUser::getUsername, SysUser::getAddress, SysUser::getAvatar, SysUser::getCreateTime,
                SysUser::getGender, SysUser::getRealName, SysUser::getCreateTime, SysUser::getPhoneNumber, SysUser::getRemark, SysUser::getStatus, SysUser::getUpdateBy, SysUser::getUpdateTime);
        // 做查询用的接口
//        lqw.like(null != name, Book::getName, name).eq(null != type && !"".equals(type), Book::getType, type);

        IPage<SysUser> bookIPage = sysUserService.page(page, lqw);

        return new Result(Code.GET_OK, bookIPage, "用户信息获取成功！");
    }

    @GetMapping("/self")
    public Result userSelf() {
        Integer user_id = (Integer) request.getAttribute("user_id");
        //从redis中获取用户信息
        String redisKey = "login:" + user_id;

        LoginUser loginUser = redisCache.getCacheObject(redisKey);
        SysUser user = loginUser.getUser();
        user.setPassword(null);
        user.setStatus(null);

        return new Result(Code.GET_OK, user, "个人信息获取成功！");
    }

    // 删除用户
    @DeleteMapping
    @PreAuthorize("hasAuthority('sys:user:del')")
    public Result roleDel(@RequestBody List<String> idList) {
        boolean result;
        try {
            result = sysUserService.removeByIds(idList);
        } catch (Exception e) {
            throw new BusinessException(Code.DELETE_ERR, "删除用户失败！");
        }
        return new Result(result ? Code.DELETE_OK : Code.DELETE_ERR, result, result ? "删除用户成功！" : "删除用户失败！");
    }



}
