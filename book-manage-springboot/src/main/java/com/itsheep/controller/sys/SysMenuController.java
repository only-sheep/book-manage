package com.itsheep.controller.sys;

import cn.hutool.core.map.MapUtil;
import com.itsheep.controller.BaseController;
import com.itsheep.entity.sys.NavMenu;
import com.itsheep.entity.sys.SysMenu;
import com.itsheep.entity.sys.SysUser;
import com.itsheep.exception.BusinessException;
import com.itsheep.service.SysMenuService;
import com.itsheep.service.SysUserService;
import com.itsheep.utils.RedisCache;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends BaseController<SysMenu> {

    // 菜单导航接口
    @GetMapping("/nav")
    public Result navList() {
        Integer id = (Integer) request.getAttribute("user_id");
        List<NavMenu> navList = sysMenuService.navTree(id);
        List<String> permissions = loginService.getPermissions(id);

        return new Result(Code.GET_OK, MapUtil.builder().put("authorities", permissions).put("nav", navList).map(), "菜单列表获取成功！");
    }

    // 给角色提供的menu列表接口
    @GetMapping("/role")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result menuListRole() {
        Integer id = (Integer) request.getAttribute("user_id");
        List<SysMenu> menuList = sysMenuService.menuTreeRole(id);
        return new Result(Code.GET_OK, menuList, "菜单权限列表获取成功！");
    }


    // 菜单列表接口
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:menu:list')")
    public Result menuList() {
        Integer id = (Integer) request.getAttribute("user_id");
        List<SysMenu> menuList = sysMenuService.menuTree(id);
        return new Result(Code.GET_OK, menuList, "菜单权限列表获取成功！");
    }

    @PostMapping
    @PreAuthorize("hasAuthority('sys:menu:add')")
    public Result menuAdd(@RequestBody SysMenu sysMenu) {
        boolean result;
        try {
            result = sysMenuService.saveMenu(sysMenu);
        } catch (Exception e) {
            throw new BusinessException(Code.SAVE_ERR, "添加菜单失败！");
        }
        return new Result(result ? Code.SAVE_OK : Code.SAVE_ERR, result, result ? "添加菜单成功！" : "添加菜单失败！");
    }

    @PutMapping
    @PreAuthorize("hasAuthority('sys:menu:edit')")
    public Result menuUpdate(@RequestBody SysMenu sysMenu) {
        boolean result;
        try {
            result = sysMenuService.updateMenu(sysMenu);
        } catch (Exception e) {
            throw new BusinessException(Code.UPDATE_ERR, "修改菜单失败！");
        }
        return new Result(result ? Code.UPDATE_OK : Code.UPDATE_ERR, result, result ? "修改菜单成功!" : "修改菜单失败！");
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('sys:menu:del')")
    public Result menuDelete(@PathVariable("id") Integer user_id) {
        boolean result = false;
        try {
            result = sysMenuService.removeById(user_id);
        } catch (Exception e) {
            throw new BusinessException(Code.DELETE_ERR, "删除菜单失败！");
        }
        return new Result(result ? Code.DELETE_OK : Code.DELETE_ERR, result, result ? "删除菜单成功！" : "删除菜单失败！");
    }
}
