package com.itsheep.controller;

import com.itsheep.service.*;
import com.itsheep.utils.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

public class BaseController<T> {

    @Resource
    public SysMenuService sysMenuService;

    @Resource
    public HttpServletRequest request; //自动注入request

    @Resource
    public SysUserService sysUserService;

    @Resource
    public SysRoleService sysRoleService;

    @Resource
    public LoginService loginService;

    @Resource
    public RedisCache redisCache;

    @Resource
    public BookService bookService;

    @Resource
    public BookLendRecordService bookLendRecordService;

}
