package com.itsheep.interceptor;

import com.alibaba.druid.util.StringUtils;
//import com.auth0.jwt.JWT;
//import com.auth0.jwt.exceptions.AlgorithmMismatchException;
//import com.auth0.jwt.exceptions.SignatureVerificationException;
//import com.auth0.jwt.exceptions.TokenExpiredException;
import com.itsheep.exception.BusinessException;
import com.itsheep.service.SysUserService;
import com.itsheep.utils.controller.Code;
import com.itsheep.utils.controller.JwtUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@Component
public class JWTInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
//            System.out.println("OPTIONS请求，放行");
            return true;
        }
        String token = request.getHeader("token");
        if (StringUtils.isEmpty(token)) {
            throw new BusinessException(Code.LOGIN_ERR, "token不能为空");
        }

        Claims claims = JwtUtil.parseJWT(token);
        if (claims == null) {
            throw new BusinessException(Code.LOGIN_ERR, "token异常");
        }
        if (JwtUtil.isExpire(claims)) {
            throw new JwtException("token过期");
        }

        return true;
    }
}
